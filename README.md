# Sensing The Environment (and sending data)

Here we have code for Arduino MKR 1010 and Particle Argon, though
more will come.

Sensors used so far:
- Seeed Dust Sensor
- Seeed Temperature and Humidity Sensor (DHT11)
- Seeed Soil Moisture Sensor

All the sensors have a grove connection, therefore grove shields are used
for the boards. reason: easy to connect (no soldering, no other components
involved etc).

As of now (2021-05-07), the boards connect to a WiFi network and send data
to a python server running on a pc (which is connected to the network
using a static IP).

Data are then pushed to Influx database and then pulled and visualised by
a Grafana instance.
